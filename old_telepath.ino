#include <SoftwareSerial.h>
#include <Brain.h>

SoftwareSerial imp(A0, A1); // RX, TX
Brain brain(imp);
String brainData;

void setup()  
{
  // Open serial communications and wait for port to open:
  // used for writing
  Serial.begin(9600);

  // SoftwareSerial port, used for reading
  // set baud to match arduino
  imp.begin(9600);
}

void loop() // run over and over
{
  // read from sofware serial pin, 
  // write over bluetooth virtual serial
  if (brain.update()) {
    Serial.println(brain.readCSV());
  }
}
