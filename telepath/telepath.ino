#include <SoftwareSerial.h>
#include <Brain.h>

/*

Attach TX on the HC-05 to Arduino pin 4
Attach RX on the HC-05 to Arduino pin 5

*/

SoftwareSerial brain_serial(10, 11); // RX, TX
SoftwareSerial command_serial(12, 13); // RX, TX
Brain brain(brain_serial);

void setup()  
{
  // SoftwareSerial port, used for reading
  // set baud to match arduino
  brain_serial.begin(9600);
  
  // Open serial communications and wait for port to open:
  // used for writing
  //Serial.begin(9600);

  // Used to communicate with master over bluetooth
  command_serial.begin(38400);
}

void loop() // run over and over
{
  // read from sofware serial pin, 
  // write over bluetooth virtual serial
  if (brain.update()) {
    int attention = brain.readAttention();
    //Serial.println(attention);
    command_serial.println(attention);
  }
}
