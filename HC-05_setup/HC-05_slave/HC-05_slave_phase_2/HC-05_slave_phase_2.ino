#include <SoftwareSerial.h>

#define RxD 10
#define TxD 11


/*

Attach TX on the HC-05 to Arduino pin 10
Attach RX on the HC-05 to Arduino pin 11

*/

SoftwareSerial BTSerial(RxD, TxD);

void setup(){
  BTSerial.begin(38400);
  Serial.begin(9600);
  pinMode(7, OUTPUT);
  pinMode(6, OUTPUT);
}

void loop() {
    if (BTSerial.available()){
      int number = BTSerial.parseInt();
      Serial.println(number);
      if (number >= 25){
        digitalWrite(6, HIGH);
        if (number >= 75){
          digitalWrite(7, HIGH);
        }
        else {
          digitalWrite(7, LOW);
        }
      }
      else {
        digitalWrite(6, LOW);
      }
    }
    
  //if (Serial.available())
  //  BTSerial.write(Serial.read());
}
