#include <SoftwareSerial.h>

#define RxD 10
#define TxD 11


/*

Attach TX on the HC-05 to Arduino pin 10
Attach RX on the HC-05 to Arduino pin 11

Use the following procedure:
 1 Press and hold the button
 2 Remove VCC module
 3 Return to power the module
 4 Release the button
 5 The module will blink slowly, indicating that the AT mode
 6 Reset Arduino
*/

SoftwareSerial BTSerial(RxD, TxD);

void setup(){
  BTSerial.begin(38400);
  Serial.begin(9600);
  Serial.println ( " If you see OK output below - >> The device is ready .");
  Serial.println ( " Otherwise - >> The device is not in AT mode. ");
  Serial.println("-------------------------------------------------------------------");
  delay(2000);
  BTSerial.print("AT+ORGL\r\n");
  delay(400);
  BTSerial.print("AT+BIND=\r\n");
  delay(400);
  BTSerial.print("AT+ROLE=0\r\n");     // Set mode to Slave
  delay(400);
  BTSerial.print("AT+ADDR?\r\n");      // Show address
}

void loop(){

  if (BTSerial.available())
    Serial.write(BTSerial.read());

  if (Serial.available())
    BTSerial.write(Serial.read());

}

