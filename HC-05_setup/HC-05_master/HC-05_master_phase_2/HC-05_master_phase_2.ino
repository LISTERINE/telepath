#include <SoftwareSerial.h>

#define RxD 10
#define TxD 11


/*

Attach TX on the HC-05 to Arduino pin 10
Attach RX on the HC-05 to Arduino pin 11

Use the following procedure:
 1 Press and hold the button
 2 Remove VCC module
 3 Return to power the module
 4 Release the button
 5 The module will blink slowly, indicating that the AT mode
 6 Reset Arduino

Make sure you have run the HC-05_master_phase_1 sketch already to put your device into master mode.
These AT commands should be sticky, and persist after powering off.


*/

SoftwareSerial BTSerial(RxD, TxD);

void setup(){
  BTSerial.begin(38400);
  Serial.begin(9600);
  Serial.println ( " If you see OK output below - >> The device is ready .");
  Serial.println ( " Otherwise - >> The device is not in AT mode. ");
  Serial.println("-------------------------------------------------------------------");
  delay(2000);
  BTSerial.print("AT\r\n");      // Clear any paired devices
  delay(400);
  BTSerial.print("AT+ROLE?\r\n");     // Show mode set to Master, should print 1
  delay(400);
  
  BTSerial.print("AT+BIND=2015,8,132909\r\n"); // Bind to slave
  delay(400);
  BTSerial.print("AT+BIND?\r\n"); // Confirm bound address
}

void loop(){

  if (BTSerial.available())
    Serial.write(BTSerial.read());

  if (Serial.available())
    BTSerial.write(Serial.read());

}

