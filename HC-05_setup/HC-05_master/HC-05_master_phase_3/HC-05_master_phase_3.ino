#include <SoftwareSerial.h>

#define RxD 10
#define TxD 11


/*

Attach TX on the HC-05 to Arduino pin 10
Attach RX on the HC-05 to Arduino pin 11

*/

SoftwareSerial BTSerial(RxD, TxD);

void setup(){
  BTSerial.begin(38400);
  Serial.begin(9600);
}

void loop() {
  //if (Serial.available())
  //for (int i; i < 10; i++){
    BTSerial.write(1);
    delay(1000);
  //}
}
