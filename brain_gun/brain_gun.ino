#include <SoftwareSerial.h>

#define RxD 10
#define TxD 11
/*

Attach TX on the HC-05 to Arduino pin 10
Attach RX on the HC-05 to Arduino pin 11

*/

// motor to push darts into flywheel
int pusher = 13;
// flywheel motor to accelerate darts
int flywheel = 12;

// Serial to recieve commands over bluetooth
SoftwareSerial BTSerial(RxD, TxD);

void setup()
{
  // set all the motor control pins to outputs
  pinMode(pusher, OUTPUT);
  pinMode(flywheel, OUTPUT);
  BTSerial.begin(38400);
  Serial.begin(9600);
}

// Push darts into flywheels for one second
void push()
{
  digitalWrite(pusher, HIGH);
  delay(1000);
  digitalWrite(pusher, LOW);
}

// Spin up flywheels for three seconds, fire, spin flywheels for two more seconds to clear any darts, spin down.
void fire()
{
  digitalWrite(flywheel, HIGH);
  delay(3000);
  push();
  delay(2000);
  digitalWrite(flywheel, LOW);
}


void loop()
{
  if (BTSerial.available()){
      int number = BTSerial.parseInt();
      Serial.println(number);
      if (number >= 80){
        fire();
      }
    }
    delay(100);
}
